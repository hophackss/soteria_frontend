(function () {
  'use strict';

  angular.module('hophacks2016App')
    .service('BackendService', function ($http) {
      return {
        getUserLocations: getUserLocations,
        getLogging: getLogging
      };


      function getUserLocations() {
        return $http.get('http://52.72.190.94:5000/userLocation');
      }

      function getLogging() {
        return $http.get('http://52.72.190.94:5000/userLocationLog');
      }
    });
})();

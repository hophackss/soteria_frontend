(function () {
  'use strict';

  angular.module('hophacks2016App')
    .service('MapsModel', function (BackendService, $interval, moment, _) {

      var model;

      function MapsModel() {
        model = this;

        model.map = {
          center: {
            latitude: 39.3289013, longitude: -76.6205177
          },
          zoom: 15,
          options: {
            streetViewControl: false,
            mapTypeControl: false
          }
        };
        model.destination = [];
        model.path = [];

        model.filterStudents = filterStudents;
        model.filterGuards = filterGuards;
        model.onClick = onClick;
        model.closeDetail = closeDetail;
        model.clickLog = clickLog;
        model.selectUser = selectUser;

        refreshLog();
        $interval(refreshLog, 1000);

        refreshMarkers().then(function () {
          model.enableStudents = true;
          model.enableGuards = true;
          filterStudents();
          filterGuards();
        });
        $interval(refreshMarkers, 1000);
        return model;
      }

      function onClick(marker, eventName, markerModel) {
        model.destination = [];
        model.path = [];
        if (!model.details || markerModel.deviceId !== model.details.deviceId) {
          model.details = markerModel;
          selectUser(markerModel);
        } else {
          model.details = null;
        }
      }

      function selectUser(user) {
        if (user) {
          if (user.destination) {
            model.destination = user.destination ? [user.destination] : [];
            model.path = [{
              path: [
                user.coords,
                model.destination[0].coords
              ],
              stroke: {
                color: '#6060FB',
                weight: 3
              },
              id: 2
            }];
          }
          model.map.center = _.cloneDeep(user.coords);
        } else {
          model.destination = [];
          model.path = [];
        }
      }

      function closeDetail() {
        model.destination = [];
        model.details = null;
        model.path = [];
      }

      function refreshMarkers() {
        return BackendService.getUserLocations().then(function (userLocations) {
          var markers = _.map(userLocations.data, function (marker) {
            if (typeof marker.emergency === 'string') {
              marker.emergency = marker.emergency === 'true';
            }
            if (typeof marker.enroute === 'string') {
              marker.enroute = marker.enroute === 'true';
            }
            marker.icon = getIcon(marker.userInfo.userType, marker.emergency);
            if (marker.destination) {
              marker.destination.icon = getIcon(marker.destination.type);
              marker.destination.id = 1;
            }
            return marker;
          });
          model.markers = markers;
          model.filteredMarkers = markers;
          filterStudents();
          filterGuards();
        });
      }

      function filterStudents() {
        if (model.enableStudents) {
          model.filteredMarkers = _.uniq(model.filteredMarkers.concat(_.filter(model.markers, function (marker) {
            return _.lowerCase(marker.userInfo.userType) === 'student';
          })));
        } else {
          model.filteredMarkers = _.filter(model.filteredMarkers, function (marker) {
            return _.lowerCase(marker.userInfo.userType) !== 'student';
          });
          model.destination = [];
          model.path = [];
          if (model.details && _.lowerCase(model.details.userInfo.userType) === 'student') {
            model.details = null;
          }
        }
      }

      function filterGuards() {
        if (model.enableGuards) {
          model.filteredMarkers = _.uniq(model.filteredMarkers.concat(_.filter(model.markers, function (marker) {
            return _.lowerCase(marker.userInfo.userType) === 'security';
          })));
        } else {
          model.filteredMarkers = _.filter(model.filteredMarkers, function (marker) {
            return _.lowerCase(marker.userInfo.userType) !== 'security';
          });
          if (model.details && _.lowerCase(model.details.userInfo.userType) === 'security') {
            model.details = null;
          }
        }
      }

      function getIcon(type, emergency) {
        switch (_.lowerCase(type)) {
          case 'student':
            if (emergency) {
              return 'images/student_emergency.png';
            }
            return 'images/student_track.png';
          case 'security':
            return 'images/security.png';
          case 'home':
            return 'images/home.png';
          case 'campus':
            return 'images/campus.png';
          default:
            return '';
        }
      }

      function refreshLog() {
        BackendService.getLogging().then(function (logging) {
          model.logMessages = _.map(logging.data.reverse().slice(0, Math.min(25, logging.data.length)), function (record) {
            record.timestamp = moment(record.timestamp);
            return record;
          });
        });
      }

      function clickLog(deviceId) {
        model.details = _.find(model.markers, {deviceId: deviceId});
        selectUser(model.details);
      }

      return MapsModel;
    });

})();

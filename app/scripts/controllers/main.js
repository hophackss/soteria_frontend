(function () {
  'use strict';

  /**
   * @ngdoc function
   * @name hophacks2016App.controller:MainCtrl
   * @description
   * # MainCtrl
   * Controller of the hophacks2016App
   */
  angular.module('hophacks2016App')
    .controller('MainCtrl', function ($scope, MapsModel) {
      $scope.mapsModel = new MapsModel();
    });
})();

(function () {
  'use strict';

  angular.module('hophacks2016App')
    .directive('logger', function () {
      return {
        restrict: 'E',
        scope: {
          model: '='
        },
        templateUrl: '../views/logger.html',
        controller: controller,
        controllerAs: 'loggerVm',
        bindToController: true
      };


      function controller($scope) {
        var vm = this;

        vm.model = $scope.loggerVm.model;

        return vm;
      }
    });

})();

(function () {
  'use strict';

  angular.module('hophacks2016App')
    .directive('settings', function () {
      return {
        restrict: 'E',
        scope: {
          model: '='
        },
        templateUrl: '../views/settings.html',
        controller: controller,
        controllerAs: 'settingsVm',
        bindToController: true
      };


      function controller ($scope) {
        var vm = this;

        vm.model = $scope.settingsVm.model;

        return vm;
      }
    });

})();

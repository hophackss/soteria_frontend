(function () {
  'use strict';

  angular.module('hophacks2016App')
    .directive('maps', function () {
      return {
        restrict: 'E',
        scope: {
          model: '='
        },
        templateUrl: '../views/maps.html',
        controller: controller,
        controllerAs: 'mapsVm',
        bindToController: true
      };

      function controller($scope) {
        var vm = this;

        vm.model = $scope.mapsVm.model;

        $scope.$watch('mapsVm.model.details', function (details) {
          vm.model.selectUser(details);
        });

        return vm;
      }
    });
})();

(function () {
  'use strict';

  /**
   * @ngdoc overview
   * @name hophacks2016App
   * @description
   * # hophacks2016App
   *
   * Main module of the application.
   */
  angular
    .module('hophacks2016App', [
      'ngAnimate',
      'ngCookies',
      'ngResource',
      'ngRoute',
      'ngSanitize',
      'ngTouch',
      'uiGmapgoogle-maps',
      'angularMoment'
    ])
    .constant('_', window._)
    .config(function ($routeProvider) {
      $routeProvider
        .when('/', {
          templateUrl: 'views/main.html',
          controller: 'MainCtrl',
          controllerAs: 'main'
        })
        .otherwise({
          redirectTo: '/'
        });
    });
})();
